﻿using Masd.EAutoService.MechanicAppUSvc.Model;
using Masd.EAutoService.MechanicAppUSvc.Rest.Client;
using Masd.EAutoService.MechanicAppUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicAppUSvc.Logic
{

    public class OrdersData: IOrdersData
    {
        private static readonly object orderLock = new object();

        private static ICustomersDataService _customerClient;
        private static IServicesDataService _serviceClient;
        private static IMechanicsDataService _mechanicClient;
        private static IOrdersDataService _orderClient;


        static OrdersData()
        {
            lock (orderLock)
            {
                _customerClient = new CustomersDataServiceClient();
                _serviceClient = new ServicesDataServiceClient();
                _mechanicClient = new MechanicsDataServiceClient();
                _orderClient = new OrdersDataServiceClient();
                
                //_customerClient = new MockCustomersDataServiceClient();
                //_serviceClient = new MockServicesDataServiceClient();
                //_mechanicClient = new MockMechanicsDataServiceClient();
                //_orderClient = new MockOrdersDataServiceClient();
            }
        }

        public Order[] GetOrders()
        {
            lock (orderLock)
            {
                OrderConverter converter = new OrderConverter(_mechanicClient,
                                              _customerClient,
                                              _orderClient,
                                              _serviceClient);

                Order[] convertedOrders = converter.Convert().ToArray();
                return convertedOrders;
            }

        }
        public Order GetOrder(int id)
        {
            lock (orderLock)
            {
                OrderConverter converter = new OrderConverter(_mechanicClient,
                                              _customerClient,
                                              _orderClient,
                                              _serviceClient);

                Order[] convertedOrders = converter.Convert().ToArray();
                return convertedOrders.FirstOrDefault(m => m.Id == id);
            }
        }

        public void EditOrder(int id, bool isFinished)
        {
            lock (orderLock)
            {
                _orderClient.EditOrder(id, isFinished);
            }
        }

        public void AddServicesToOrder(int id, List<int> services)
        {
            lock (orderLock)
            {


                List<int> filteredServicesId = new List<int>();

                foreach(int serviceId in services)
                {
                    if (_serviceClient.GetServices().Any(p => p.ServiceId == serviceId) )
                    {
                        filteredServicesId.Add(serviceId);
                    }
                }

                _orderClient.AddServicesToOrder(id, filteredServicesId);
                    
            }
        }
        public ServiceDTO[] GetServices()
        {
            lock (orderLock)
            {
                return _serviceClient.GetServices();
            }
        }
    }
}