﻿using Masd.EAutoService.MechanicAppUSvc.Model;
using Masd.EAutoService.MechanicAppUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicAppUSvc.Logic
{
    public class OrderConverter
    {
        private OrderDTO[] _ordersDTO;
        List<Order> orders = new List<Order>();

        IMechanicsDataService MechanicClient;

        ICustomersDataService CustomerClient;

        IOrdersDataService OrderClient;

        IServicesDataService ServiceClient;

        

        public OrderConverter(IMechanicsDataService mechanicClient, 
                              ICustomersDataService customerClient, 
                              IOrdersDataService orderClient, 
                              IServicesDataService serviceClient)
        {
            MechanicClient = mechanicClient;
            CustomerClient = customerClient;
            OrderClient = orderClient;
            ServiceClient = serviceClient;
            _ordersDTO = OrderClient.GetOrders();
        }

        public List<Order> Convert()
        {
            foreach (OrderDTO orderDTO in _ordersDTO)
            {
                List<String> serviceList = new List<String>();
                foreach(int serviceId in orderDTO.ServiceList)
                {
                    serviceList.Add(ServiceClient.GetService(serviceId).Name);
                }

                Order order = new Order(orderDTO.Id, 
                                        MechanicClient.GetMechanic(orderDTO.MechanicId).Name,
                                        MechanicClient.GetMechanic(orderDTO.MechanicId).Surname,
                                        CustomerClient.GetCustomer(orderDTO.CustomerId).Name,
                                        CustomerClient.GetCustomer(orderDTO.CustomerId).Surname,
                                        orderDTO.IsFinished,
                                        serviceList                                        
                                        );
                orders.Add(order);
            }

            return orders;
        }
    }
}
