using Masd.EAutoService.MechanicAppUSvc.Logic;
using Masd.EAutoService.MechanicAppUSvc.Model;
using Masd.EAutoService.MechanicAppUSvc.Rest.Model;
using Microsoft.AspNetCore.Mvc;

namespace Masd.EAutoService.MechanicAppUSvc.Rest.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class OrdersAppController : ControllerBase, IOrdersData
    {
        private readonly ILogger<OrdersAppController> logger;
        private readonly IOrdersData orderRepo;

        public OrdersAppController(ILogger<OrdersAppController> logger)
        {
            this.logger = logger;
            orderRepo = new OrdersData();
        }

        [HttpGet]
        [Route("GetOrders")]
        public Order[] GetOrders()
        {
            return orderRepo.GetOrders();
        }

        [HttpGet]
        [Route("GetOrder")]
        public Order GetOrder(int id)
        {
            return orderRepo.GetOrder(id);
        }

        [HttpPut]
        [Route("EditOrder")]
        public void EditOrder(int id, bool isFinished)
        {
            orderRepo.EditOrder(id, isFinished);
        }

        [HttpPut]
        [Route("AddServicesToOrder")]
        public void AddServicesToOrder(int id, List<int> services)
        {
            orderRepo.AddServicesToOrder(id, services);
        }
        [HttpGet]
        [Route("GetServices")]
        public ServiceDTO[] GetServices()
        {       
            return orderRepo.GetServices();
        }

    }
}