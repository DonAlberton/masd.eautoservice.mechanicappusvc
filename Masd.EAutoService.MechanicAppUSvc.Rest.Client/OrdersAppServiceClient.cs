﻿using System.Text.Json;
using Masd.EAutoService.MechanicAppUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicAppUSvc.Rest.Client
{
    public class OrdersAppServiceClient : IOrdersAppService
    {
        public static int port = 9004;
        private static readonly HttpClient httpClient = new HttpClient();
        public OrderAppDTO[] GetOrders()
        {
            string webServiceUrl = $"https://localhost:{port}/OrdersApp/GetOrders";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            OrderAppDTO[] mechanicsData = ConvertJson(jsonResponseContent);

            return mechanicsData;
        }
        public OrderAppDTO GetOrder(int id)
        {
            string webServiceUrl = $"https://localhost:{port}/OrdersApp/GetOrder?id={id}";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            OrderAppDTO mechanicsData = ConvertJsonObject(jsonResponseContent);

            return mechanicsData;
        }

        private async Task<string> CallWebService(HttpMethod httpMethod, string webServiceUrl)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }

        private OrderAppDTO[] ConvertJson(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            OrderAppDTO[] mechanicsData = JsonSerializer.Deserialize<OrderAppDTO[]>(json, options);

            return mechanicsData;
        }

        private OrderAppDTO ConvertJsonObject(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            OrderAppDTO mechanicsData = JsonSerializer.Deserialize<OrderAppDTO>(json, options);

            return mechanicsData;
        }
        public void EditOrder(int id, bool isFinished)
        {
            string webServiceUrl = $"https://localhost:{port}/OrdersData/EditOrder?id={id}&isFinished={isFinished}";


            Task<string> webServiceCall = CallWebService(HttpMethod.Put, webServiceUrl);

            webServiceCall.Wait();

        }

        public void AddServicesToOrder(int id, List<int> services)
        {
            string webServiceUrl = $"https://localhost:{port}/OrdersData/AddServicesToOrder?id={id}";
            Task<string> webServiceCall = CallWebServiceWithBody(HttpMethod.Put, webServiceUrl, services);

            webServiceCall.Wait();
        }

        private async Task<string> CallWebServiceWithBody(HttpMethod httpMethod, string webServiceUrl, List<int> body)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);
            string requestBody = JsonSerializer.Serialize(body);
            HttpResponseMessage httpResponseMessage = await httpClient.PutAsync(webServiceUrl, new StringContent(requestBody, System.Text.Encoding.UTF8, "application/json"));

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }

        public ServiceDTO[] GetServices()
        {
            string webServiceUrl = String.Format("https://{0}:{1}/OrdersApp/GetServices", "localhost", port);

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            ServiceDTO[] servicesData = ConvertJsonServices(jsonResponseContent);

            return servicesData;

        }

        private ServiceDTO[] ConvertJsonServices(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            ServiceDTO[] servicesData = JsonSerializer.Deserialize<ServiceDTO[]>(json, options)!;

            return servicesData;
        }
    }
}
