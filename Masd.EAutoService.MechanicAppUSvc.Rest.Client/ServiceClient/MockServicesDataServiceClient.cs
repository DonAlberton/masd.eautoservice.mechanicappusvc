﻿using Masd.EAutoService.MechanicAppUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicAppUSvc.Rest.Client
{
    public class MockServicesDataServiceClient : IServicesDataService
    {
        public static ServiceDTO[] servicesDTO = new ServiceDTO[] 
        { 
            new ServiceDTO { ServiceId = 1, Name = "Oil change", Price = 20.00 }, 
            new ServiceDTO { ServiceId = 2, Name = "Filter Change", Price = 15.25 }, 
            new ServiceDTO { ServiceId = 3, Name = "Breaks Change", Price = 19.99 }, 
            new ServiceDTO { ServiceId = 4, Name = "Tires change", Price = 9.99 } 
        };
        public ServiceDTO GetService(int id)
        {
            return servicesDTO.FirstOrDefault(m => m.ServiceId == id)!;
        }

        public ServiceDTO[] GetServices()
        {
            return servicesDTO;
        }
    }
}