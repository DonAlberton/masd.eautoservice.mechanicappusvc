﻿using Masd.EAutoService.MechanicAppUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicAppUSvc.Rest.Client
{
    public class MockCustomersDataServiceClient : ICustomersDataService
    {
        public static CustomerDTO[] customersDTO = new CustomerDTO[] { new CustomerDTO { Id = 1, Name = "Michal", Surname = "Kruk" }, new CustomerDTO { Id = 1, Name = "Michal", Surname = "Baron" }, new CustomerDTO { Id = 2, Name = "Zbigniew", Surname = "Robak" }, new CustomerDTO { Id = 3, Name = "Robert", Surname = "Prawy" } };
        public CustomerDTO GetCustomer(int searchText)
        {
            return customersDTO.FirstOrDefault(m => m.Id == searchText);
        }

        public CustomerDTO[] GetCustomers()
        {
            return customersDTO;
        }

        public int GetCustomerId()
        {
            return customersDTO.Last().Id;
        }

        public void AddCustomer(string customerName, string customerSurname)
        {
            CustomerDTO customer = new CustomerDTO { Id = customersDTO.Last().Id + 1, Name = customerName, Surname = customerSurname };
            customersDTO.Append(customer);
        }
    }
}