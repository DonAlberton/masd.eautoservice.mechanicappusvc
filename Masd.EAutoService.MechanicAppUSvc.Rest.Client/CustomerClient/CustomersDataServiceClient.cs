﻿using System.Text.Json;
using Masd.EAutoService.MechanicAppUSvc.Rest.Model;
using System.Net.Http.Headers;

namespace Masd.EAutoService.MechanicAppUSvc.Rest.Client
{
    public class CustomersDataServiceClient : ICustomersDataService
    {
        private static readonly HttpClient httpClient = new HttpClient();

        private static readonly int port = 80;
        private static readonly string ipAddress = "192.168.50.104";
        
        /*private static string ipAddress = "localhost";
        private static int port = 8084;*/
        public CustomerDTO GetCustomer(int searchText)
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/CustomerData/GetCustomer?id={searchText}";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            CustomerDTO customersData = ConvertJson1(jsonResponseContent);

            return customersData;
        }

        public CustomerDTO[] GetCustomers()
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/CustomerData/GetCustomers";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            CustomerDTO[] customersData = ConvertJson(jsonResponseContent);

            return customersData;
        }

        private async Task<string> CallWebService(HttpMethod httpMethod, string webServiceUrl)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }
        private async Task<string> CallWebService2(HttpMethod httpMethod, string webServiceUrl)
        {


            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, webServiceUrl);

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(request);


            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }
        public void AddCustomer(string customerName, string customerSurname)
        {

            string webServiceUrl = $"http://{ipAddress}:{port}/CustomerData/AddCustomer?customerName={customerName}&customerSurname={customerSurname}";

            Task<string> webServiceCall = CallWebService2(HttpMethod.Post, webServiceUrl);

            webServiceCall.Wait();

        }



        private CustomerDTO[] ConvertJson(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

            CustomerDTO[] customersData = JsonSerializer.Deserialize<CustomerDTO[]>(json, options)!;

            return customersData;
        }
        private CustomerDTO ConvertJson1(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

            CustomerDTO customersData = JsonSerializer.Deserialize<CustomerDTO>(json, options)!;

            return customersData;
        }

        public int GetCustomerId()
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/CustomerData/GetCustomers";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            int customersData = int.Parse(jsonResponseContent);

            return customersData;
        }
    }
}