﻿using Masd.EAutoService.MechanicAppUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicAppUSvc.Rest.Client
{
    public class MockMechanicsDataServiceClient : IMechanicsDataService
    {
        public static MechanicDTO[] mechanicsDTO = new MechanicDTO[] { new MechanicDTO { Id = 1, Name = "Adam", Surname = "Kazimierczuk" }, new MechanicDTO { Id = 2, Name = "Marek", Surname = "Czaplin" }, new MechanicDTO { Id = 3, Name = "Mariusz", Surname = "Pudzianowski" }, new MechanicDTO { Id = 4, Name = "Radek", Surname = "Tradek" } };
        public MechanicDTO[] GetMechanics()
        {
            return mechanicsDTO;
        }

        public MechanicDTO GetMechanic(int id)
        {
            return mechanicsDTO.FirstOrDefault(m => m.Id == id);
        }

        public int GetMechanicId()
        {
            return mechanicsDTO.Last().Id;
        }

    }
}