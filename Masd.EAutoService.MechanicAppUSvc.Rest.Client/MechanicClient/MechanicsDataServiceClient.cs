﻿using System.Net.Http;
using System.Text.Json;
using Masd.EAutoService.MechanicAppUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicAppUSvc.Rest.Client
{
    public class MechanicsDataServiceClient : IMechanicsDataService
    {
        private static readonly HttpClient httpClient = new HttpClient();
        
        private static int port = 80;
        private static string ipAddress = "192.168.50.101";

        /*private static string ipAddress = "localhost";
        private static int port = 8081;*/
        public MechanicDTO[] GetMechanics()
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/MechanicsData/GetMechanics";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            MechanicDTO[] mechanicsData = ConvertJson(jsonResponseContent);

            return mechanicsData;
        }

        public MechanicDTO GetMechanic(int id)
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/MechanicsData/GetMechanic?id={id}";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            MechanicDTO mechanicsData = ConvertJsonObject(jsonResponseContent);

            return mechanicsData;
        }

        private async Task<string> CallWebService(HttpMethod httpMethod, string webServiceUrl)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }
        private MechanicDTO[] ConvertJson(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            MechanicDTO[] mechanicsData = JsonSerializer.Deserialize<MechanicDTO[]>(json, options);

            return mechanicsData;
        }
        private MechanicDTO ConvertJsonObject(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            MechanicDTO mechanicsData = JsonSerializer.Deserialize<MechanicDTO>(json, options);

            return mechanicsData;
        }
        public int GetMechanicId()
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/MechanicsData/GetMechanicId";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            int mechanicsData = int.Parse(jsonResponseContent);

            return mechanicsData;
        }
    }
}