﻿using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using Masd.EAutoService.MechanicAppUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicAppUSvc.Rest.Client
{
    public class OrdersDataServiceClient : IOrdersDataService
    {
        private static readonly HttpClient httpClient = new HttpClient();
        
        public static readonly int port = 80;
        private static string ipAddress = "192.168.50.102";

        /*private static string ipAddress = "localhost";
        private static int port = 8082;*/
        public OrderDTO[] GetOrders()
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersData/GetOrders";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            OrderDTO[] ordersData = ConvertJson(jsonResponseContent);

            return ordersData;
        }

        public OrderDTO GetOrder(int id)
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersData/GetOrder?id={id}";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            OrderDTO ordersData = ConvertJsonSingle(jsonResponseContent);

            return ordersData;
        }

        private async Task<string> CallWebService(HttpMethod httpMethod, string webServiceUrl)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }

        private async Task<string> CallWebServiceWithBody(HttpMethod httpMethod, string webServiceUrl, List<int> body)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);
            string requestBody = JsonSerializer.Serialize(body);
            HttpResponseMessage httpResponseMessage = await httpClient.PutAsync(webServiceUrl, new StringContent(requestBody, System.Text.Encoding.UTF8, "application/json"));

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }

        private async Task<string> CallWebService2(HttpMethod httpMethod, string webServiceUrl, StringContent content)
        {


            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

            HttpResponseMessage httpResponseMessage = await httpClient.PostAsync(webServiceUrl, content);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }


        public void AddOrder(int mechanicId, int customerId, List<int> servicesList)
        {

            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersData/AddOrder?mechanicId={mechanicId}&customerId={customerId}";

            String listParameter = JsonSerializer.Serialize(servicesList);

            StringContent content = new StringContent(listParameter, Encoding.UTF8, "application/json");

            Task<string> webServiceCall = CallWebService2(HttpMethod.Post, webServiceUrl, content);

            webServiceCall.Wait();

        }

        public void EditOrder(int id, bool isFinished)
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersData/EditOrder?id={id}&isFinished={isFinished}";


            Task<string> webServiceCall = CallWebService(HttpMethod.Put, webServiceUrl);

            webServiceCall.Wait();

        }
        public void AddServicesToOrder(int id, List<int> services)
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersData/AddServicesToOrder?id={id}";
            Task<string> webServiceCall = CallWebServiceWithBody(HttpMethod.Put, webServiceUrl, services);

            webServiceCall.Wait();
        }

        private OrderDTO[] ConvertJson(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            OrderDTO[] ordersData = JsonSerializer.Deserialize<OrderDTO[]>(json, options);

            return ordersData;
        }
        private OrderDTO ConvertJsonSingle(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            OrderDTO ordersData = JsonSerializer.Deserialize<OrderDTO>(json, options);

            return ordersData;
        }
        public int GetOrderId()
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersData/GetOrderId";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            int ordersData = int.Parse(jsonResponseContent);

            return ordersData;
        }
    }
}