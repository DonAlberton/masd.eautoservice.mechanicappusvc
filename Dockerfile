FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src

EXPOSE 80

COPY . .

RUN dotnet restore

RUN dotnet build "Masd.EAutoService.MechanicAppUSvc.Rest" -c Debug -o /app/build

FROM build AS publish

RUN dotnet publish "Masd.EAutoService.MechanicAppUSvc.Rest/Masd.EAutoService.MechanicAppUSvc.Rest.csproj" -c Debug -o /app/publish

FROM base AS final
WORKDIR /app


COPY --from=publish /app/publish .


ENTRYPOINT ["dotnet", "Masd.EAutoService.MechanicAppUSvc.Rest.dll"]
