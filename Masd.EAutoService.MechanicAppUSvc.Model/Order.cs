﻿namespace Masd.EAutoService.MechanicAppUSvc.Model
{
    public class Order
    {
        public Order(int id, string mechanicName, string mechanicSurrname, string customerName, string customerSurrname, bool isFinished, List<string> serviceList)
        {
            Id = id;
            MechanicName = mechanicName;
            MechanicSurname = mechanicSurrname;
            CustomerName = customerName;
            CustomerSurname = customerSurrname;
            IsFinished = isFinished;
            ServiceList = serviceList;
        }

        public int Id { get; set; }
        public string MechanicName { get; set; }
        public string MechanicSurname { get; set; }
        public string CustomerName { get; set; }
        public string CustomerSurname { get; set; }
        public bool IsFinished { get; set; }
        public List<String> ServiceList { get; set; }
    }
}