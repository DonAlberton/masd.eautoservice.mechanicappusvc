﻿using Masd.EAutoService.MechanicAppUSvc.Rest.Model;

namespace Masd.EAutoService.MechanicAppUSvc.Model
{
    public interface IOrdersData
    {
        public Order[] GetOrders();
        public Order GetOrder(int id);
        public void EditOrder(int id, bool isFinished);
        public void AddServicesToOrder(int id, List<int> services);
        public ServiceDTO[] GetServices();
    }
}
