﻿namespace Masd.EAutoService.MechanicAppUSvc.Rest.Model
{
    public class ServiceDTO
    {
        public int ServiceId { get; set; }
        public string? Name { get; set; }
        public double Price { get; set; }
    }
}