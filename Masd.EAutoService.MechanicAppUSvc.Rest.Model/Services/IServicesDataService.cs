﻿namespace Masd.EAutoService.MechanicAppUSvc.Rest.Model
{
    public interface IServicesDataService
    {
        ServiceDTO GetService(int id);
        ServiceDTO[] GetServices();
    }
}
