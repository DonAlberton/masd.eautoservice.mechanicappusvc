﻿namespace Masd.EAutoService.MechanicAppUSvc.Rest.Model
{
    public class OrderAppDTO
    {
        public int Id { get; set; }
        public String MechanicName { get; set; }
        public String MechanicSurname { get; set; }
        public String CustomerName { get; set; }
        public String CustomerSurname { get; set; }
        public bool IsFinished { get; set; }
        public List<String> ServiceList { get; set; }
    }
}
