﻿namespace Masd.EAutoService.MechanicAppUSvc.Rest.Model
{
    public interface IMechanicsDataService
    {
        public MechanicDTO[] GetMechanics();
        public MechanicDTO GetMechanic(int id);
        public int GetMechanicId();
    }
}