﻿namespace Masd.EAutoService.MechanicAppUSvc.Rest.Model
{
    public class MechanicDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public override string ToString()
        {
            return $"id: {Id}, name: {Name}, surrname: {Surname}";
        }
    }
}