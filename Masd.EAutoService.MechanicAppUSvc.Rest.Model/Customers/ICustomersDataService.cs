﻿namespace Masd.EAutoService.MechanicAppUSvc.Rest.Model
{
    public interface ICustomersDataService
    {
        public CustomerDTO GetCustomer(int id);
        public CustomerDTO[] GetCustomers();
        public void AddCustomer(string customerName, string customerSurname);

        public int GetCustomerId();
    }
}
