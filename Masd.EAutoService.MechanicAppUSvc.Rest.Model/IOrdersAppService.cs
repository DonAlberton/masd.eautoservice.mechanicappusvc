﻿namespace Masd.EAutoService.MechanicAppUSvc.Rest.Model
{
    public interface IOrdersAppService
    {
        public OrderAppDTO[] GetOrders();
        public OrderAppDTO GetOrder(int id);
        public void EditOrder(int id, bool isFinished);
        public void AddServicesToOrder(int id, List<int> services);
        public ServiceDTO[] GetServices();
    }
}
